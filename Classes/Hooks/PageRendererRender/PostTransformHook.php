<?php
namespace HIVE\HiveHooks\Hooks\PageRendererRender;

/**
 * @author Benjamin Kott <info@bk2k.info>
 */
class PostTransformHook {

    /**
     * @param array $params
     * @param \TYPO3\CMS\Core\Page\PageRenderer $pagerenderer
     */
    public function execute(&$params, &$pagerenderer){
        if(TYPO3_MODE !== 'FE' || !is_array($params['jsLibs'])) {
            return;
        }

        /*
         * Add async option to libs
         */
        foreach ($params['jsLibs'] as $sKey => $sValue) {
            /*
             * only for concatendated / merged libs
             */
            if (strpos($sKey,'/merged') !== FALSE ) {
                $params['jsLibs'][$sKey]["async"] = true;
            }

        }
    }
}